#!/usr/bin/env python3

greet = "Hello Python"
greet_second = 'Hello Python'
escaped_greet = "Hello\tWorld"      # Tab (\t) used in b/w the strings

qoutes = "You can fool all the people some of the time, \
and some of the people all the time, \
but you cannot fool all the people all the time."

# This a docstring or tripple quotes string
qoutes_triple_q = """You can fool all the people some of the time,
and some of the people all the time,
but you cannot fool all the people all the time."""

# String methods
print(qoutes.upper())
print(qoutes.count('a'))    # Find the occurrence of letter (a) in the quotes

# Find the length of the string
print(len(qoutes))

# Formating string

print(f"Once Abraham Lincoln was said \"{qoutes}\"")

# Concatenations
# It's is a method to combines 2 or more string
print("hello " + "world")


# Indexing
print(qoutes[0])

# Slicing
print(qoutes[0:3])
