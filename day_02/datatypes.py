#!/usr/bin/env python3

# A data type is a classification of data which tells the 
# compiler or interpreter how the programmer intends to use the data.
# Python support various types of data, including integer, real
# string, Boolean and more.

int_type = 33

string_type = "Hello World"

float_point = 2341.134

bool_type = True
bool_type2 = False

complex_number = 1j
