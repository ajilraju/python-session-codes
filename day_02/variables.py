#!/usr/bin/env python3

# A variable is a way of storing information in a computer program.
# Python variable creation syntax.
# variable_name = value

my_age = 26
my_name = "Tom"

# print() is a function used to print value of the object to consoles.

print(my_age)
print(my_name)
