#!/usr/bin/env python3

shopping_list = ['Apple', 'Orange', 'Milk', 'Rice', 'Book', 'Pen']

print(f"Which item should I buy first?, {shopping_list[0]}")

total_number = len(shopping_list)
print(f"Which item should I buy last?, {shopping_list[total_number - 1]}")

total_number = len(shopping_list)
print(f"Which item should I buy last?, {shopping_list[-1]}")
