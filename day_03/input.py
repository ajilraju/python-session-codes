#!/usr/bin/env python3

# input() builtin used for reading value from keyboard and it's return str from
# the function.

user_name = input("Enter your name: ")
age = int(input("Enter your age: "))    # int() type-casting str -> int


print(f"Hello.... {user_name}, Have nice a day")
print(f"Your age + 100 is {age + 100}")
