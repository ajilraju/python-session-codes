#!/usr/bin/env python3

age = 26
more_age = 10

# Arithmetic operators
print(f"After adding my age with more_age, I get {age + more_age}")
print(f"After subtract my age with more_age, I get {age - more_age}")

# Comparison operator
print(f"I'm an adult? {age > 18}")
